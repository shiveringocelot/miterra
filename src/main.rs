#![feature(int_roundings)]

extern crate piston_window;
extern crate vecmath;
extern crate camera_controllers;
#[macro_use]
extern crate gfx;
extern crate shader_version;

use piston_window::*;
use camera_controllers::{
    FirstPersonSettings,
    FirstPerson,
    CameraPerspective,
    model_view_projection
};

mod render;
mod chunks;
mod voxel_source;
mod mesher;
mod smoother;

pub use mesher::Mesher;
pub use render::{Mesh, Vertex};

pub const WIREFRAME: bool = false;


pub type Matrix = [[f32; 4]; 4];

pub struct MeshData {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
}

impl MeshData {
    pub fn new () -> Self { Self { vertices: vec![], indices: vec![] } }
    pub fn build (&self, factory: &mut render::Factory) -> Mesh {
        Mesh::new(&self.vertices[..], &self.indices[..], factory)
    }
}

pub struct App {
    window: PistonWindow,
    renderer: render::Renderer,
    first_person: FirstPerson,

    meshes: Vec<Mesh>,
}

impl App {
    fn render (&mut self, _args: RenderArgs) {
        let projection = self.get_projection();
        self.renderer.render(&mut self.window, &self.meshes, render::RenderArgs {
            view: self.first_person.camera(_args.ext_dt).orthogonal(), projection,
        });
    }

    fn get_projection (&self) -> Matrix {
        let draw_size = self.window.draw_size();
        CameraPerspective {
            fov: 90.0, near_clip: 0.1, far_clip: 1000.0,
            aspect_ratio: (draw_size.width as f32) / (draw_size.height as f32)
        }.projection()
    }

    fn update (&mut self, _args: UpdateArgs) {
        // this can be done per resize
        /*let projection = self.get_projection();

        for mesh in &mut self.meshes {
            mesh.data.u_model_view_proj = model_view_projection(
                mesh.transform,
                self.first_person.camera(args.dt).orthogonal(),
                projection
            );
        }*/
    }
}

fn main() {

    let opengl = OpenGL::V3_2;

    let mut window: PistonWindow =
        WindowSettings::new("piston: cube", [640, 480])
        .exit_on_esc(true)
        .samples(4)
        .graphics_api(opengl)
        .build()
        .unwrap();

    window.set_capture_cursor(true);

    let mut factory = window.factory.clone();

    let vertex_data = vec![
        //top (0, 0, 1)
        Vertex::new_tex([-1.0, -1.0,  1.0], [0.0, 0.0]),
        Vertex::new_tex([ 1.0, -1.0,  1.0], [1.0, 0.0]),
        Vertex::new_tex([ 1.0,  1.0,  1.0], [1.0, 1.0]),
        Vertex::new_tex([-1.0,  1.0,  1.0], [0.0, 1.0]),
        //bottom (0, 0, -1)
        Vertex::new_tex([ 1.0,  1.0, -1.0], [0.0, 0.0]),
        Vertex::new_tex([-1.0,  1.0, -1.0], [1.0, 0.0]),
        Vertex::new_tex([-1.0, -1.0, -1.0], [1.0, 1.0]),
        Vertex::new_tex([ 1.0, -1.0, -1.0], [0.0, 1.0]),
        //right (1, 0, 0)
        Vertex::new_tex([ 1.0, -1.0, -1.0], [0.0, 0.0]),
        Vertex::new_tex([ 1.0,  1.0, -1.0], [1.0, 0.0]),
        Vertex::new_tex([ 1.0,  1.0,  1.0], [1.0, 1.0]),
        Vertex::new_tex([ 1.0, -1.0,  1.0], [0.0, 1.0]),
        //left (-1, 0, 0)
        Vertex::new_tex([-1.0,  1.0,  1.0], [0.0, 0.0]),
        Vertex::new_tex([-1.0, -1.0,  1.0], [1.0, 0.0]),
        Vertex::new_tex([-1.0, -1.0, -1.0], [1.0, 1.0]),
        Vertex::new_tex([-1.0,  1.0, -1.0], [0.0, 1.0]),
        //front (0, 1, 0)
        Vertex::new_tex([-1.0,  1.0, -1.0], [0.0, 0.0]),
        Vertex::new_tex([ 1.0,  1.0, -1.0], [1.0, 0.0]),
        Vertex::new_tex([ 1.0,  1.0,  1.0], [1.0, 1.0]),
        Vertex::new_tex([-1.0,  1.0,  1.0], [0.0, 1.0]),
        //back (0, -1, 0)
        Vertex::new_tex([ 1.0, -1.0,  1.0], [0.0, 0.0]),
        Vertex::new_tex([-1.0, -1.0,  1.0], [1.0, 0.0]),
        Vertex::new_tex([-1.0, -1.0, -1.0], [1.0, 1.0]),
        Vertex::new_tex([ 1.0, -1.0, -1.0], [0.0, 1.0]),
    ];

    let index_data: &[u16] = &[
         0,  1,  2,  2,  3,  0, // top
         4,  6,  5,  6,  4,  7, // bottom
         8,  9, 10, 10, 11,  8, // right
        12, 14, 13, 14, 12, 15, // left
        16, 18, 17, 18, 16, 19, // front
        20, 21, 22, 22, 23, 20, // back
    ];

    let cube_mesh = (MeshData {
        vertices: vertex_data,
        indices: index_data.to_vec(),
    }).build(&mut factory);

    //let sphere = voxel_source::SphereSource { x: 32, y: -16, z: 32, r: 32+16 };
    let sphere = voxel_source::RippleSource {
        amplitude: 1.0 / 64.0,
        magnitude: 2.1,
        bias: 6.0,
    };

    const SIZE: i32 = 128;

    enum Mode { Blocky, SurfNet, Smooth, Sphere }
    let mode = Mode::Smooth;

    let voxel_mesh_data = match mode {
        Mode::Blocky => {
            let mut mesher = mesher::Blocky { size: SIZE };
            mesher.mesh(&sphere)
        },
        Mode::SurfNet => {
            let mut mesher = mesher::SurfNet { size: SIZE as u16, smooth: 4 };
            mesher.mesh(&sphere)
        },
        Mode::Smooth => {
            let mut mesher = mesher::MarchingCubes { size: SIZE, smooth: false };


            let now = ::std::time::Instant::now();
            let mut smooth = smoother::Smoother::new(sphere, SIZE);
            /*smooth.fill();
            //smooth.print_slice();
            smooth.smooth();
            smooth.smooth();
            smooth.smooth();
            smooth.smooth();
            //smooth.print_slice();
            */

            let tm = now.elapsed();
            println!("The voxels were smoothed in {} ms, with {} voxels",
                (tm.as_secs()*1000) + (tm.subsec_nanos()/1_000_000) as u64,
                SIZE * SIZE * SIZE
            );

            mesher.mesh(&smooth)
        },
        Mode::Sphere => {
            struct ScalarSphereSource { x: i32, y: i32, z: i32, r: i32, }
            impl voxel_source::VoxelSource<f32> for ScalarSphereSource {
              fn get(&self, ix: i32, iy: i32, iz: i32) -> f32 {
                let (x, y, z) = (ix-self.x, iy-self.y, iz-self.z);
                let d2 = x*x + y*y + z*z;
                self.r as f32 - (d2 as f32).sqrt()
              }
            }

            let smooth = ScalarSphereSource { x: 32, y: 0, z: 32, r: 32 };
            let mut mesher = mesher::MarchingCubes { size: 64, smooth: false };
            mesher.mesh(&smooth)
        }
    };

    let voxel_mesh = voxel_mesh_data.build(&mut factory);

    let mut app = App {
        window,
        renderer: render::Renderer::new(factory),
        first_person: FirstPerson::new(
            [32.0, 28.0, 48.0],
            FirstPersonSettings::keyboard_wasd()
        ),
        meshes: vec![cube_mesh, voxel_mesh],
    };

    app.first_person.settings.speed_horizontal = 5.0;
    app.first_person.settings.speed_vertical = 5.0;

    while let Some(e) = app.window.next() {
        app.first_person.event(&e);

        if let Some(args) = e.render_args() {
            app.render(args);
        }

        if let Some(args) = e.update_args() {
            app.update(args);
        }

        /*if e.resize_args().is_some() {
            for mesh in &mut app.meshes {
                mesh.data.out_color = app.window.output_color.clone();
                mesh.data.out_depth = app.window.output_stencil.clone();
            }
        }*/
    }
}
