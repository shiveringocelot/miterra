
pub trait VoxelSource<T> {
  fn get(&self, x: i32, y: i32, z: i32) -> T;
}

pub struct SphereSource {
  pub x: i32,
  pub y: i32,
  pub z: i32,
  pub r: i32,
}

impl VoxelSource<bool> for SphereSource {
  fn get(&self, ix: i32, iy: i32, iz: i32) -> bool {
    let (x, y, z) = (ix-self.x, iy-self.y, iz-self.z);
    let d2 = x*x + y*y + z*z;
    let r2 = self.r * self.r;
    return d2 < r2;
  }
}

pub struct SineSource {
  pub amplitude: f32,
  pub magnitude: f32,
  pub bias: f32,
}

impl VoxelSource<bool> for SineSource {
  fn get(&self, x: i32, y: i32, z: i32) -> bool {
    let xv = (x as f32 * self.amplitude).cos();
    let zv = (z as f32 * self.amplitude).cos();
    let v = xv*zv*self.magnitude + self.bias;
    return y < v as i32;
  }
}

pub struct RippleSource {
  pub amplitude: f32,
  pub magnitude: f32,
  pub bias: f32,
}

impl VoxelSource<bool> for RippleSource {
  fn get(&self, x: i32, y: i32, z: i32) -> bool {
    let v = ((x*x + z*z) as f32 * self.amplitude).sqrt().cos();
    let v = v*self.magnitude + self.bias;
    return y < v as i32;
  }
}

pub struct ScalarWrapper <S> {
  pub source: S,
  pub low: f32,
  pub high: f32,
}

impl<S: VoxelSource<bool>> VoxelSource<f32> for ScalarWrapper<S> {
  fn get(&self, x: i32, y: i32, z: i32) -> f32 {
    if self.source.get(x, y, z) { self.high } else { self.low }
  }
}