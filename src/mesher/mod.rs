
use crate::{MeshData, voxel_source::VoxelSource};

mod blocky;
mod surfnet;
mod marching_cubes;

pub use blocky::Blocky;
pub use surfnet::SurfNet;
pub use marching_cubes::MarchingCubes;

pub trait Mesher<T> {
  fn mesh (&mut self, source: &impl VoxelSource<T>) -> MeshData;
}

pub fn calculate_normals (mesh: &mut MeshData) {
  type Vector3 = vecmath::Vector3<f32>;

  use vecmath::vec3_sub as sub;
  fn add_vec (a: &mut Vector3, b: Vector3) {
    *a = vecmath::vec3_add(*a, b);
  }

  if (mesh.indices.len() < 1) { return; }

  let high = mesh.indices.len()/3-1;

  // Reset all normals
  for vertex in mesh.vertices.iter_mut() {
    vertex.normal = [0.0, 0.0, 0.0];
  }

  for ii in 0 .. high {
    let i = ii*3;

    let aa = mesh.indices[i  ] as usize;
    let bb = mesh.indices[i+1] as usize;
    let cc = mesh.indices[i+2] as usize;

    let a = mesh.vertices[aa].pos;
    let b = mesh.vertices[bb].pos;
    let c = mesh.vertices[cc].pos;

    // To get a weighted sum, do not normalize this
    let n = vecmath::vec3_cross(sub(b, a), sub(c, a));

    add_vec(&mut mesh.vertices[aa].normal, n);
    add_vec(&mut mesh.vertices[bb].normal, n);
    add_vec(&mut mesh.vertices[cc].normal, n);
  }

  // Normalize all normals
  for vertex in mesh.vertices.iter_mut() {
    vertex.normal = vecmath::vec3_normalized(vertex.normal);
  }
}