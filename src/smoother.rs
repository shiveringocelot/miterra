
use std::cell::RefCell;

use crate::voxel_source::VoxelSource;
use crate::chunks::Chunks;

/*
Surface Extraction from Binary Volumes with Higher-Order Smoothness
Victor Lempitsky

https://www.robots.ox.ac.uk/~vgg/publications/2010/Lempitsky10/lempitsky10.pdf
*/

const LEVELS: usize = 1;

/*
#[derive(Copy, Clone, Default)]
struct Voxel {
    // Levels known
    known: u8,
    // known if known > 0
    bool_val: bool,
    // known if known > 1
    boundary: bool,
    levels: [f32; LEVELS]
}

struct Chunk {
    voxels: Vec<Voxel>,
}

impl Chunk {
    fn get_level <S: VoxelSource<bool>> (&mut self, source: &S, size: i32, level: u8, x: i32, y: i32, z: i32) -> f32 {
        if x < 0 || y < 0 || z < 0 || x >= size || y >= size || z >= size {
            return 0.0;
        }

        let index = (x + y*size + z*size*size) as usize;
        
        let voxel = &mut self.voxels[index];

        if voxel.known == 0 {
            voxel.bool_val = source.get(x, y, z);
            voxel.levels[0] = if voxel.bool_val { 1.0 } else { -1.0 };
            voxel.known = 1;
        }

        if level == 0 { return voxel.levels[0]; }

        if voxel.known > 1 {
            if voxel.boundary {
                return voxel.levels[0];
            } else {
                return voxel.levels[1];
            }
        } else {
            let val = voxel.levels[0];

            let neighbors = [
                self.get_level(source, size, 0, x-1, y, z),
                self.get_level(source, size, 0, x, y-1, z),
                self.get_level(source, size, 0, x, y, z-1),
                self.get_level(source, size, 0, x+1, y, z),
                self.get_level(source, size, 0, x, y+1, z),
                self.get_level(source, size, 0, x, y, z+1),
            ];

            let voxel = &mut self.voxels[index];
            for n in neighbors {
                if n != val {
                    voxel.boundary = true;
                    return val;
                }
            }

            voxel.boundary = false;
            voxel.levels[1] = val;
            voxel.known = 2;
            return val;
        }
    }
}
*/

struct Chunk {
    size: usize,
    data: Vec<f32>,
}

impl Chunk {
    pub fn new (mut size: usize) -> Self {
        size += LEVELS*2;
        Self { size, data: vec![0.0; size*size*size] }
    }

    fn process (&mut self, source: &impl VoxelSource<bool>) {
        self.fill(source);
        for i in 0..LEVELS {
            self.smooth(i as i32);
        }
    }

    fn fill (&mut self, source: &impl VoxelSource<bool>) {
        let sz = self.size as usize;
        let off = LEVELS as i32;
        for x in 0..sz {
            for y in 0..sz {
                for z in 0..sz {
                    let b = source.get(x as i32 - off, y as i32 - off, z as i32 - off);
                    let f = if b { 1.0 } else { -1.0 };
                    self.data[x + y*sz + z*sz*sz] = f;
                }
            }
        }
    }

    fn index (&self, x: i32, y: i32, z: i32) -> Option<usize> {
        let s = self.size as i32;
        if x >= s || y >= s || z >= s || x < 0 || y < 0 || z < 0 {
            None
        } else {
            Some((x + y*s + z*s*s) as usize)
        }
    }

    fn smooth (&mut self, skip: i32) {
        fn solve (x: f32, a: f32, b: f32, c: f32) -> f32 {
            a*x*x + b*x + c
        }

        fn minimize (a: f32, b: f32, c: f32) -> f32 {
            if a == 0.0 {
                if b > 0.0 { f32::INFINITY }
                else if b < 0.0 { f32::NEG_INFINITY }
                else { 0.0 }
            } else {
                b / a * -0.5
            }
        }

        let mut solved = vec![0.0; self.data.len()];
        let high = self.size as i32 - skip;

        for x in skip .. high {
            for y in skip .. high {
                for z in skip .. high {
                    let dx = self.get(x-1, y, z) + self.get(x+1, y, z);
                    let dy = self.get(x, y-1, z) + self.get(x, y+1, z);
                    let dz = self.get(x, y, z-1) + self.get(x, y, z+1);

                    let a = 12.0;
                    let b = (dx + dy + dz) * -4.0;
                    let c = dx*dx + dy*dy + dz*dz;

                    let prev = self.get(x, y, z);
                    let next = minimize(a, b, c);

                    solved[self.index(x, y, z).unwrap()] = next;
                }
            }
        }

        self.data = solved;
    }

    pub fn blur (&mut self) {

        let mut solved = vec![0.0; self.data.len()];
        let sz = self.size as i32;

        for x in 0..sz {
            for y in 0..sz {
                for z in 0..sz {
                    solved[self.index(x, y, z).unwrap()] = (
                        self.get(x+1, y, z) +
                        self.get(x, y+1, z) +
                        self.get(x, y, z+1) +
                        self.get(x-1, y, z) +
                        self.get(x, y-1, z) +
                        self.get(x, y, z-1) +
                        self.get(x, y, z)
                    ) / 7.0;
                }
            }
        }

        self.data = solved;
    }

    pub fn print_slice (&self) {
        use std::fmt::Write;
        let mut s = String::new();
        for y in 28..36 {
            for x in 28..36 {
                write!(&mut s, "{:.1} ", self.get(x, y, 32));
            }
            write!(&mut s, "\n");
        }
        println!("{}", s);
    }

    fn get (&self, x: i32, y: i32, z: i32) -> f32 {
        let off = LEVELS as i32;
        match self.index(x, y, z) {
            Some(i) => self.data[i],
            None => 0.0,
        }
    }
}

struct Inner <S> {
    source: S,
    size: i32,
    chunks: Chunks<Chunk>,
}

impl <S: VoxelSource<bool>> Inner<S> {
    fn get_level (&mut self, level: u8, x: i32, y: i32, z: i32) -> f32 {

        let s = self.size as usize;
        let (entry, (lx, ly, lz)) = self.chunks.entry_at(x, y, z);
        let chunk = entry.or_insert_with(|| {
            //let mut chunk = Chunk { voxels: vec![Voxel::default(); s*s*s] };
            let mut chunk = Chunk::new(self.size as usize);
            chunk.process(&self.source);
            chunk
        });

        let off = LEVELS as i32;

        return chunk.get(x + off, y + off, z + off);
    }
}

pub struct Smoother<S>(RefCell<Inner<S>>);

impl<S> Smoother<S> {
    pub fn new (source: S, size: i32) -> Self {
        Self(RefCell::new(Inner {
            source, size,
            chunks: Chunks::new(size),
        }))
    }
}


/*
impl<S: VoxelSource<bool>> Smoother<S> {
    pub fn new (source: S, size: usize) -> Self {
        Self {
            source, size,
            data: vec![0.0; size*size*size],
        }
    }

    pub fn fill (&mut self) {
        let sz = self.size;
        for x in 0..sz {
            for y in 0..sz {
                for z in 0..sz {
                    let b = self.source.get(x as i32, y as i32, z as i32);
                    let f = if b { 1.0 } else { -1.0 };
                    self.data[x + y*sz + z*sz*sz] = f;
                }
            }
        }
    }

    fn index (&self, x: i32, y: i32, z: i32) -> Option<usize> {
        let s = self.size as i32;
        if x >= s || y >= s || z >= s || x < 0 || y < 0 || z < 0 {
            None
        } else {
            Some((x + y*s + z*s*s) as usize)
        }
    }

    pub fn smooth (&mut self) {
        fn solve (x: f32, a: f32, b: f32, c: f32) -> f32 {
            a*x*x + b*x + c
        }

        fn minimize (a: f32, b: f32, c: f32) -> f32 {
            if a == 0.0 {
                if b > 0.0 { f32::INFINITY }
                else if b < 0.0 { f32::NEG_INFINITY }
                else { 0.0 }
            } else {
                b / a * -0.5
            }
        }

        let mut solved = vec![0.0; self.data.len()];
        let sz = self.size as i32;

        for x in 0..sz {
            for y in 0..sz {
                for z in 0..sz {
                    let dx = self.get(x-1, y, z) + self.get(x+1, y, z);
                    let dy = self.get(x, y-1, z) + self.get(x, y+1, z);
                    let dz = self.get(x, y, z-1) + self.get(x, y, z+1);

                    let a = 12.0;
                    let b = (dx + dy + dz) * -4.0;
                    let c = dx*dx + dy*dy + dz*dz;

                    let prev = self.get(x, y, z);
                    let next = minimize(a, b, c);

                    solved[self.index(x, y, z).unwrap()] = next;
                }
            }
        }

        self.data = solved;
    }

    pub fn blur (&mut self) {

        let mut solved = vec![0.0; self.data.len()];
        let sz = self.size as i32;

        for x in 0..sz {
            for y in 0..sz {
                for z in 0..sz {
                    solved[self.index(x, y, z).unwrap()] = (
                        self.get(x+1, y, z) +
                        self.get(x, y+1, z) +
                        self.get(x, y, z+1) +
                        self.get(x-1, y, z) +
                        self.get(x, y-1, z) +
                        self.get(x, y, z-1) +
                        self.get(x, y, z)
                    ) / 7.0;
                }
            }
        }

        self.data = solved;
    }

    pub fn print_slice (&self) {
        use std::fmt::Write;
        let mut s = String::new();
        for y in 28..36 {
            for x in 28..36 {
                write!(&mut s, "{:.1} ", self.get(x, y, 32));
            }
            write!(&mut s, "\n");
        }
        println!("{}", s);
    }

    fn get_level (&self) {

    }
}
*/

impl<S: VoxelSource<bool>> VoxelSource<f32> for Smoother<S> {
    fn get(&self, x: i32, y: i32, z: i32) -> f32 {
        self.0.borrow_mut().get_level(LEVELS as u8 - 1, x, y, z)
        /*match self.index(x, y, z) {
            Some(i) => self.data[i],
            None => {
                let b = self.source.get(x, y, z);
                if b { 1.0 } else { -1.0 }
            }
        }*/
    }
}