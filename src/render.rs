use piston_window::*;
use gfx::traits::*;

use camera_controllers::model_view_projection;

pub use gfx_device_gl::{Factory, Resources};
use gfx::Factory as GfxFactory;

use super::Matrix;

gfx_vertex_struct!(Vertex {
    pos: [f32; 3] = "a_pos",
    normal: [f32; 3] = "a_normal",
    uv: [f32; 2] = "a_uv",
});

impl Vertex {
    pub fn new(x: f32, y: f32, z: f32) -> Vertex {
        Vertex::from_pos([x, y, z])
    }

    pub fn new_tex(pos: [f32; 3], uv: [f32; 2]) -> Vertex {
        Vertex { pos, uv, normal: [0.0, 0.0, 0.0] }
    }

    pub fn from_pos(pos: [f32; 3]) -> Vertex {
        Vertex { pos , uv: [0.0, 0.0], normal: [0.0, 0.0, 0.0] }
    }
}

use gfx; // ? macro bug
gfx_pipeline!( pipe {
    vbuf: gfx::VertexBuffer<Vertex> = (),
    u_model_view_proj: gfx::Global<[[f32; 4]; 4]> = "u_model_view_proj",
    t_color: gfx::TextureSampler<[f32; 4]> = "t_color",
    out_color: gfx::RenderTarget<gfx::format::Srgba8> = "o_Color",
    out_depth: gfx::DepthTarget<gfx::format::DepthStencil> =
        gfx::preset::depth::LESS_EQUAL_WRITE,
});

pub struct Mesh {
    transform: Matrix,
    vbuf: gfx::handle::Buffer<Resources, Vertex>,
    slice: gfx::Slice<Resources>,
}

impl Mesh {
    pub fn new (vertices: &[Vertex], indices: &[u16], factory: &mut Factory) -> Self {
        let (vbuf, slice) = factory.create_vertex_buffer_with_slice(vertices, indices);

        Self {
            transform: vecmath::mat4_id(),
            vbuf, slice,
        }
    }
}

pub struct Renderer {
    sampler: gfx::texture::SamplerInfo,
    texture: gfx::handle::ShaderResourceView<Resources, [f32; 4]>,
    factory: Factory,
    pso: gfx::PipelineState<Resources, pipe::Meta>,
}

pub struct RenderArgs {
    pub view: Matrix,
    pub projection: Matrix,
}

impl Renderer {
    pub fn new (mut factory: Factory) -> Self {
        let pso = {
            /* GLSL version Selection:
            use shader_version::Shaders;
            use shader_version::glsl::GLSL;

            let opengl = OpenGL::V3_2;

            let glsl = opengl.to_glsl()
            let vs = Shaders::new(
                    .set(GLSL::V1_50, include_str!("../assets/cube_150.glslv"))
                    .get(glsl).unwrap().as_bytes(),
            */

            let vs = include_bytes!("../assets/cube_150_v.glsl");
            let ps = include_bytes!("../assets/cube_150_f.glsl");

            // Geometry shader necessary for texture selection by voxel material
            // factory.create_shader_set_geometry(vs, gs, ps).unwrap();

            let shader_set = factory.create_shader_set(vs, ps).unwrap();

            let init = pipe::new();

            let prim = gfx::Primitive::TriangleList;
            let raster = if crate::WIREFRAME {
                gfx::state::Rasterizer {
                    front_face: gfx::state::FrontFace::CounterClockwise,
                    cull_face: gfx::state::CullFace::Nothing,
                    method: gfx::state::RasterMethod::Line(2),
                    offset: None,
                    samples: None,
                }
            } else {
                gfx::state::Rasterizer::new_fill().with_cull_back()
            };


            factory.create_pipeline_state(&shader_set, prim, raster, init).unwrap()
        };

        let texture = {
            // Create texture from CPU, instead of loading it from a file
            let texels = [
                [0xff, 0xff, 0xff, 0x00],
                [0xff, 0x00, 0x00, 0x00],
                [0x00, 0xff, 0x00, 0x00],
                [0x00, 0x00, 0xff, 0x00]
            ];

            let (_, texture_view) = factory.create_texture_immutable::<gfx::format::Rgba8>(
                gfx::texture::Kind::D2(2, 2, gfx::texture::AaMode::Single),
                gfx::texture::Mipmap::Provided,
                &[&texels]).unwrap();
            texture_view
        };

        Renderer {
            pso, texture, factory,
            sampler: gfx::texture::SamplerInfo::new(
                gfx::texture::FilterMethod::Bilinear,
                gfx::texture::WrapMode::Clamp
            ),
        }
    }

    pub fn render (&mut self, window: &mut PistonWindow, meshes: &[Mesh], args: RenderArgs) {
        if meshes.len() < 1 { return; }

        window.window.make_current();

        window.encoder.clear(&window.output_color, [0.3, 0.3, 0.3, 1.0]);
        window.encoder.clear_depth(&window.output_stencil, 1.0);

        let mut data = pipe::Data {
            vbuf: meshes[0].vbuf.clone(),
            u_model_view_proj: [[0.0; 4]; 4],
            t_color: (self.texture.clone(), window.factory.create_sampler(self.sampler)),
            out_color: window.output_color.clone(),
            out_depth: window.output_stencil.clone(),
        };

        for mesh in meshes {
            data.vbuf = mesh.vbuf.clone();
            data.u_model_view_proj = model_view_projection(mesh.transform, args.view, args.projection);

            window.encoder.draw(&mesh.slice, &self.pso, &data);
        }

        window.encoder.flush(&mut window.device);
    }
}
