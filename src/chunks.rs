
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Coord { pub x: i32, pub y: i32, pub z: i32 }

pub struct Chunks<T> {
    size: i32,
    chunks: HashMap<Coord, T>,
}

pub type Entry<'a, T> = std::collections::hash_map::Entry<'a, Coord, T>;

impl<T> Chunks<T> {
    pub fn new (size: i32) -> Self {
        Chunks { size, chunks: HashMap::new() }
    }

    pub fn get (&self, x: i32, y: i32, z: i32) -> Option<&T> {
        let coord = Coord { x, y, z };
        self.chunks.get(&coord)
    }

    pub fn entry (&mut self, x: i32, y: i32, z: i32) -> Entry<'_, T> {
        self.chunks.entry(Coord { x, y, z })
    }

    pub fn entry_at (&mut self, x: i32, y: i32, z: i32) -> (Entry<'_, T>, (i32, i32, i32)) {
        let s = self.size;
        let c = Coord { x: x.div_floor(s), y: y.div_floor(s), z: z.div_floor(s) };
        (self.chunks.entry(c), (x-c.x*s, y-c.y*s, z-c.z*s))
    }
}