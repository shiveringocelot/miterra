#version 150 core

uniform sampler2D t_color;

in vec3 v_normal;
in vec2 v_TexCoord;
out vec4 o_Color;

out vec4 FragColor;

const vec3 u_LightDir = normalize(vec3(0.5, 1.0, 0.3));

void main() {
    vec4 tex = texture(t_color, v_TexCoord);
    float blend = dot(v_TexCoord-vec2(0.5,0.5), v_TexCoord-vec2(0.5,0.5));

    float diff = 0.005 + max(0.0, dot(normalize(v_normal), normalize(u_LightDir)))*0.995;

    o_Color = mix(tex, vec4(0.0,0.0,0.0,0.0), blend*1.0) * diff;
    o_Color.a = 1.0;
}