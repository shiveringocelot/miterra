#version 150 core
in vec3 a_pos;
in vec3 a_normal;
in vec2 a_uv;
out vec2 v_TexCoord;
out vec3 v_normal;
uniform mat4 u_model_view_proj;
void main() {
    v_TexCoord = a_uv;
    v_normal = a_normal;
    gl_Position = u_model_view_proj * vec4(a_pos, 1.0);
}
